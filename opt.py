#!/usr/bin/python
import os
import time
import math
import random
import sys

if sys.platform.startswith("linux"):
    from Tkinter import *
if sys.platform.startswith("win32"):
    from tkinter import *
#define Tkinter on linux, tkinter on windows

width=1024
height=480

master = Tk()

master.title("Optical illusion, in Python By Bender")
        
w = Canvas(master, width=1024, height = 480)

w.pack()
#make middle lin2

for x in range(0, width):
    if(x % 2 != 0):
      w.create_line(x * 10, 0, x * 10, height + 5, width = 10)  

uno = w.create_rectangle(0, 100, 100, 120, fill="Blue")
dos = w.create_rectangle(0, 355, 100, 375, fill="Yellow")

def forward(arg):

    #call coords
    #update
    coords = w.coords(arg)

    w.coords(arg, coords[0] + .5, coords[1], coords[2] + .5, coords[3])
    master.after(10, forward, arg)


forward(uno)
forward(dos)
mainloop()

 
